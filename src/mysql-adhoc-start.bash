#!/usr/bin/env bash
############################################################################
#
# Version 0.1.0
#
# Usage: mysql-adhoc-start.bash [options]
#
# Options:
#  -h               … help message
#  -c [docker_cmd]  … specify a replacement for the default `docker`.
#                     e.g. `podman`. You can also
#                     export CMD_DOCKER=podman; ./mysql-adhoc-start.bash
#                     The chosen (or detected) implementation will change
#                     the `--userns` value (default: `--userns=host`,
#                     but podman will use `--userns=keep-id`)
#  -d [database]    … database name. Alternatively export DATABASE_NAME.
#                     Defaults to "homestead".
#  -f               … enable php compatible password (mysql native password)
#                     We do so by mounting php-compatible-password.cnf from
#                     our template folder into /etc/mysql/conf.d/.
#                     Needs to be set since mysql:8 from my experience.
#                     Alternatively export COMPATIBLE_PASSWORD_FILE with
#                     the path to a *.cnf file and skip `-p`.
#  -k               … kill existing container (remove any existing
#                     container with that name before starting)
#  -m [port]        … map 3306 to the local port specified. Alternatively
#                     export MAP_LOCAL_PORT. Defaults to 3306.
#  -n [name]        … container name. Alternatively, export CONTAINER_NAME.
#  -p [password]    … mysql password. Alternatively, export
#                     DATABASE_PASSWORD.
#                     Defaults to "secret".
#  -r               … remove datadir (delete folder and persisted data)
#                     you want to remove the datadir once you need to
#                     change the configuration (-u -p -v -f -d)
#  -t [tag]         … Image tag. E.g. 5 or 8. Defaults to 8.
#                     Think of it as the mysql version to use.
#                     Example: ./mysql-adhoc-start.bash -t 5
#  -u [user]        … mysql user name. Alternatively, export DATABASE_USER.
#                     Defaults to "homestead".
#  -v [datadir]     … path to data dir (mapped volume for persisting data)
#                     Defaults to "${PWD}/data".
#
############################################################################

# exit when a command fails
set -o errexit
# return the exit status of the last command that threw a non-zero exit code
set -o pipefail
# exit when script tries to use undeclared variables
set -o nounset

SCRIPT_PATH=$(realpath "${BASH_SOURCE[0]}")
# containing directory
SCRIPT_DIR=$(dirname "${SCRIPT_PATH}")

# shellcheck source=print.bash
source "${SCRIPT_DIR}/print.bash"

main() {
  local OPTIND OPTARG opt

  while getopts ":c:d:m:n:p:t:u:v:fhkr" opt; do
    case "${opt}" in
      h)
        print_usage
        exit
        ;;
      c)
        CMD_DOCKER="${OPTARG}"
        ;;
      d)
        DATABASE_NAME="${OPTARG}"
        ;;
      f)
        COMPATIBLE_PASSWORD_FILE="${SCRIPT_DIR}/../template/php-compatible-password.cnf"
        ;;
      k)
        KILL_EXISTING="true"
        ;;
      m)
        MAP_LOCAL_PORT="${OPTARG}"
        ;;
      n)
        CONTAINER_NAME="${OPTARG}"
        ;;
      p)
        DATABASE_PASSWORD="${OPTARG}"
        ;;
      r)
        REMOVE_DATA_DIR="true"
        ;;
      t)
        IMAGE_TAG="${OPTARG}"
        ;;
      u)
        DATABASE_USER="${OPTARG}"
        ;;
      v)
        DATA_DIR="${OPTARG}"
        ;;
      :)
        print_error "Option -${OPTARG} requires an argument."
        print_usage
        exit 1
        ;;
      \?)
        print_error "Invalid option: ${OPTARG}" >&2
        print_usage
        exit 1
        ;;
    esac
  done
  local cmd_fallback
  cmd_fallback=$(command -v docker || command -v podman)
  local cmd="${CMD_DOCKER:-$cmd_fallback}"
  local data_dir="${DATA_DIR:-${PWD}/data}"
  local user="1000"
  local group="1000"
  local userns="host"
  local mode
  mode=$(basename "$cmd")
  if [[ $mode == 'podman' ]]; then
    userns="keep-id"
  fi
  local image_name="mysql"
  local image_tag=${IMAGE_TAG:-8}
  local alias data_dir_parent
  data_dir_parent=$(dirname "${data_dir}")
  data_dir_parent=$(realpath "$data_dir_parent")
  alias=$(basename "${data_dir_parent}")
  local container_name="${CONTAINER_NAME:-${alias}-${image_name}-${image_tag}}"
  local mysql_root_password="secret"
  local mysql_database="${DATABASE_NAME:-homestead}"
  local mysql_user="${DATABASE_USER:-homestead}"
  local mysql_password="${DATABASE_PASSWORD:-secret}"
  local php_compatible_password=""
  local map_local_port="${MAP_LOCAL_PORT:-3306}"
  if [[ -n "${COMPATIBLE_PASSWORD_FILE:-}" ]]; then
    # trim leading spaces
    COMPATIBLE_PASSWORD_FILE=${COMPATIBLE_PASSWORD_FILE##*( )}
    # trim trailing spaces
    COMPATIBLE_PASSWORD_FILE=${COMPATIBLE_PASSWORD_FILE%%*( )}
    php_compatible_password="-v ${COMPATIBLE_PASSWORD_FILE}:/etc/mysql/conf.d/php-compatible-password.cnf"
  fi
  if [[ ! -d ${data_dir} ]]; then
    mkdir -p "${data_dir}"
  fi

  if [[ -n "${KILL_EXISTING:-}" ]]; then
    $cmd stop "${container_name}" || echo "${container_name} was not running."
  fi

  if [[ -n "${REMOVE_DATA_DIR:-}" ]]; then
    read -p "rm -r ${data_dir}! Are you sure? [Yy]" -n 1 -r
    echo    # (optional) move to a new line
    if [[ $REPLY =~ ^[Yy]$ ]]
    then
        rm -r "${data_dir}"
    fi
  fi

  $cmd run --rm $php_compatible_password \
       -v "${data_dir}":/var/lib/mysql \
       --userns=${userns} \
       --user "${user}":"${group}" \
       --name "${container_name}" \
       -e MYSQL_ROOT_PASSWORD="${mysql_root_password}" \
       -e MYSQL_DATABASE="${mysql_database}" \
       -e MYSQL_USER="${mysql_user}" \
       -e MYSQL_PASSWORD="${mysql_password}" \
       -p "${map_local_port}":3306 \
       -d \
       "${image_name}":"${image_tag}"

}

main "$@"
